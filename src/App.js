import React, { Component } from 'react';
import logo from './logo.svg';
import axios from 'axios';
import './App.css';
import './profile.js';



class App extends Component {
    constructor(props){
        super(props);
        this.state={
            value:'',
            data:[],
            Post:[
                {
                    poster:'riyan',
                    images:'https://static.pexels.com/photos/221471/pexels-photo-221471.jpeg',
                    like:50,
                    status:'Beach',
                    comments:[
                        {name:'asd',comment:'comment 1'},
                        {name:'asd2',comment:'comment 2'},
                    ]
                },
                {
                    poster:'riyan2',
                    images:'https://static.pexels.com/photos/221471/pexels-photo-221471.jpeg',
                    like:50,
                    status:'Beautiful',
                    comments:[
                        {name:'asd',comment:'comment 1'},
                        {name:'asd2',comment:'comment 2'},
                    ]
                },
                {
                    poster:'ricky',
                    images:'https://static.pexels.com/photos/221471/pexels-photo-221471.jpeg',
                    like:50,
                    status:'See you guys',
                    comments:[
                        {name:'asd',comment:'comment 1'},
                        {name:'asd2',comment:'comment 2'},
                    ]
                },
                {
                    poster:'hartanto',
                    images:'https://upload.wikimedia.org/wikipedia/commons/6/61/Welcome_to_Japan_2016_%2825900053476%29.jpg',
                    like:50,
                    status:'Welcome to Japan',
                    comments:[
                        {name:'asd',comment:'comment 1'},
                        {name:'asd2',comment:'comment 2'},
                    ]
                },
                {
                    poster:'rudy',
                    images:'http://h71076.www7.hp.com/EMEA/omen/laptop-gallery1.jpg',
                    like:50,
                    status:'My New Laptop',
                    comments:[
                        {name:'asd',comment:'comment 1'},
                        {name:'asd2',comment:'comment 2'},
                    ]
                },

            ],
            total:2
        }
    }

    componentWillMount()
    {
        var that=this;
        axios({
            method: 'get',
            url:'https://alpha.sgbyte.com/api/studio/my_apps',
            headers:{
                'Authorization':"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFjOTZjNTlkMDdkMmQ1NmRlYTFjNjJkNjY4OTFiMDI3NTVhZTliM2MyNjI2ZTU5YTg4YWNlMTA0MjNjMjFlNTQ5YWZiMTY4OTRlZGIyZjg0In0.eyJhdWQiOiIxIiwianRpIjoiMWM5NmM1OWQwN2QyZDU2ZGVhMWM2MmQ2Njg5MWIwMjc1NWFlOWIzYzI2MjZlNTlhODhhY2UxMDQyM2MyMWU1NDlhZmIxNjg5NGVkYjJmODQiLCJpYXQiOjE1MTcyNzg0NTMsIm5iZiI6MTUxNzI3ODQ1MywiZXhwIjoxNTQ4ODE0NDUzLCJzdWIiOiI0Iiwic2NvcGVzIjpbXX0.q5l6GTQvt-HBtFRY49vpINwW8JKyndeoSUlaycf7ViGgWoDqDzNrHzXbQSEwmv3zxGFfmNQtv1tiGzSW0X2PIT2fC5fc4UmmC5te9TNOJ5p-Xu34AhdlEkgoJCn4zKdx0Ksgv5s9NfZ5_rGXJvwgVDAWDxwoqyvxiUkH0IhB9mybgK9ptDpkjFFhX_3VdGMzGsgyDZoF80UqAzdsrk4CXNVt3X_wk0OSkDlE6uOg49Lg3unxxitESD_IwaHxhmZHkobOVat0ZkHeWWlizlKFb1WUw8vL_SmJAF57GsUBQe7PjIflEL-coNUkaVigQKl3VbMJnooyqx4yTJHdgAZEty_DxPw86wAx1BFGoze3ukX0zKE5EDK3vYc-9UGR_iL81Yijci3xEtYrVppk99HY_mdyZrTkV7zzEUPckmg0_u3zkaknqk880ii7d8j4su2uaiFfP2KMjMujyySvkFZu7lXZaoX_nwE2WDjdfS-pBIL5o1GWjOyiSPu_-0FgTz5dxfC91Y36sWy6lyLgy9kj_PR096ntLv_MmU1Trc45FfQaKAQje8LcuSYJaBXrjr35qEDFIf_QFXLwghor4p7BtsanOPnfW_-cNUfxlKCBdYfc2l8-wHhh8JFaSe2508J-cfbJcdvVEP50GIeeXe51HtDjzghzFGhZbvrtO4PN3tw",
            }
        })
            .then(function(response){
                console.log(response);
                that.setState({
                    data:response.data.data
                })
            })
            .catch(function (error) {
                console.log(error)
            });
    }

  render() {
      var rows = [];
      const posting=this.state;
      posting.Post.map(function(row){
          rows.push(
              <pre>
              <div>
                  <a href={row.poster}>{row.poster}</a>
                  <img src={row.images} className="w3-image"/>
                  <div>{row.like}</div>
                  <div>{row.status}</div>
                  {row.comments.map(function(row2){
                      {/*<div>{this.state.Post[i].comments[loopkey].name}</div>
                          <div>{this.state.Post[i].comments[loopkey].comment}</div>*/}
                      return <div>{row2.comment}</div>
                  })}
                  <hr/>
              </div>
              </pre>
          );
      })

  return (
      <div className="App">
            <header className="App-header">
          <h1 className="App-title">Welcome to My Insta</h1>
            {/*<input type="text" id="search" placeholder="Search" value={this.state.value} onChange={this.handleOutoComplete}/>*/}
      </header>
          <div className="App-list">

              <div>
                  {}
              </div>
          {rows}
          </div>
      </div>
    );

  }
}

export default App;
